/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer;

import com.blindscenes.bigrammer.domain.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.io.*;


/**
 *
 * @author cristian
 */
public class UIConsole {

    private BigramFrequencyRepository bigramFrequencyRepository;
    
    @Inject
    public UIConsole(@Named("rmiRepository") BigramFrequencyRepository bigramFrequencyRepository){
        assert bigramFrequencyRepository != null;
        this.bigramFrequencyRepository = bigramFrequencyRepository;
    }
    
    public void start(){
        
        write("Welcome to bigrammer 1.1! Please enter a duple of words.");
        write("To exit press \"q\"");
        
        while(true){
            String text = read();
            if(text.equals("q"))
                break;
            else
                searchBigram(text);
        }
    }
    
    private void searchBigram(String bigram){
        BigramFrequency bigramFrequency =  bigramFrequencyRepository.getBigramFrequency(bigram);
        if(bigramFrequency == null)
            write("No match found");
        else
            write(String.format("Bigram: \"%s\" was found %d times", bigramFrequency.getBigram(), bigramFrequency.getFrequency()));
    }
    
    private String read(){
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            return br.readLine().trim();
        } catch (IOException ex) {
            System.err.println("error reading!");
        }
        return "e";
    }
    
    private void write(String text){
        System.out.println(text);
    }
}
