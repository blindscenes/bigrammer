/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer;

import com.blindscenes.bigrammer.configuration.RegisterServices;
import com.blindscenes.bigrammer.domain.*;
import com.blindscenes.bigrammer.crosscuttingconcerns.Log;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;


/**
 *
 * @author cristian
 */
public class SocketsSampleMain {

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new RegisterServices());
        //Log log = injector.getInstance(Log.class);
        //log.Save(map);
        
        ServerService server = injector.getInstance(Key.get(ServerService.class, Names.named("socketServer")));
        server.start();
        
        UIConsole console = injector.getInstance(UIConsole.class);        
        console.start();
        
        server.stop();

    }
}
