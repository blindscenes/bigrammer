/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer;

import com.blindscenes.bigrammer.configuration.RegisterServices;
import com.blindscenes.bigrammer.domain.ServerService;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

/**
 *
 * @author cristian
 */
public class RMISampleMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws RemoteException, MalformedURLException {
        
        Injector injector = Guice.createInjector(new RegisterServices());
        
        ServerService server = injector.getInstance(Key.get(ServerService.class, Names.named("rmiServer")));
        server.start();
        
        UIConsole console = injector.getInstance(UIConsole.class);        
        console.start();
        
        server.stop();
        

    }
    
}
