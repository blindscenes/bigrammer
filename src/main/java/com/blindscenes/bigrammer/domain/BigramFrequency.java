/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain;

import java.io.Serializable;

/**
 *
 * @author cristian
 */
public class BigramFrequency implements Serializable{
    
    private  String bigram;
    private  int frequency;
    
    public BigramFrequency(String bigram, int frequency){
        this.bigram = bigram;
        this.frequency =  frequency;
    }

    /**
     * @return the bigram
     */
    public String getBigram() {
        return bigram;
    }

    /**
     * @return the frequency
     */
    public int getFrequency() {
        return frequency;
    }
}
