/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain.scanner;
import java.text.Normalizer;


/**
 *
 * @author cristian
 */
public class CleanScanner implements Scanner {

    private final Scanner scanner;
    
    public CleanScanner(Scanner scanner){
        assert scanner != null;
        this.scanner = scanner;
    }
    
    @Override
    public boolean hasNext() {
        return scanner.hasNext();
    }

    @Override
    public String next() {
        String token = scanner.next();
        token = noDiacritics(token);
        return token.toLowerCase();
    }
    
    private String noDiacritics(String text){
        if(Normalizer.isNormalized(text, Normalizer.Form.NFD))
            return text;
        text = Normalizer.normalize(text, Normalizer.Form.NFD);
        return text.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
    }
    
}
