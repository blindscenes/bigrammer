/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain.scanner;

/**
 *
 * @author cristian
 */
public class LimitScanner implements Scanner {
    
    private final java.util.Scanner scanner;
    
    public LimitScanner(String text) {
        assert text != null;
        this.scanner = new java.util.Scanner(text);
    }

    @Override
    public boolean hasNext() {
        return scanner.hasNext();
    }

    @Override
    public String next() {
        return scanner.next();
    }

}
