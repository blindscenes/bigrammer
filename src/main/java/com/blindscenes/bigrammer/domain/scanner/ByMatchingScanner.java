/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain.scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author cristian
 */
public class ByMatchingScanner implements Scanner {

    private final Pattern pattern = Pattern.compile("\\p{L}+|\\.{2,3}|\\p{Punct}|¡|¿");//find other compound punctuations
    private final Matcher matcher;

    public ByMatchingScanner(CharSequence charSequence) {
        assert charSequence != null;
        matcher = pattern.matcher(charSequence);
    }

    @Override
    public boolean hasNext() {
        return matcher.find();
    }

    @Override
    public String next() {
        return matcher.group();
    }

}
