/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain.scanner;

/**
 *
 * @author cristian
 */
public interface Scanner {
    boolean hasNext();
    String next();
}
