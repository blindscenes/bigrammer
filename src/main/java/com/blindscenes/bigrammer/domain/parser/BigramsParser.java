/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain.parser;

import com.blindscenes.bigrammer.domain.scanner.Scanner;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author cristian
 */
public class BigramsParser implements Parser {

    private final Scanner scanner;

    public BigramsParser(com.blindscenes.bigrammer.domain.scanner.Scanner scanner) {
        assert scanner != null;
        this.scanner = scanner;
    }

    @Override
    public Map<String, Integer> getBigrams() {
        Map<String, Integer> bigrams = new HashMap();
        String previousToken = null;
        while (scanner.hasNext()) {
            if (previousToken == null) {
                previousToken = scanner.next();
                continue;
            }
            String currentToken = scanner.next();
            String bigram = String.format("%s %s", previousToken, currentToken);
            addBigram(bigrams, bigram);
            previousToken = currentToken;
        }
        return bigrams;
    }

    private void addBigram(Map<String, Integer> bigrams, String bigram) {
        if (!bigrams.containsKey(bigram)) {
            bigrams.put(bigram, 1);
        } else {
            bigrams.put(bigram, bigrams.get(bigram) + 1);
        }
    }
}
