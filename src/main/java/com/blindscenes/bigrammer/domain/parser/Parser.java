/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain.parser;

import java.util.Map;

/**
 *
 * @author cristian
 */
public interface Parser {
    
    Map<String, Integer> getBigrams();
    
}
