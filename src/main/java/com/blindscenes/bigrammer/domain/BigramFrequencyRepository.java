/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.domain;

/**
 *
 * @author cristian
 */
public interface BigramFrequencyRepository {
    
    BigramFrequency getBigramFrequency(String bigram);
    
}
