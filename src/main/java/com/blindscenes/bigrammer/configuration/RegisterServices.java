/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.configuration;

import com.blindscenes.bigrammer.Infrastructure.sockets.BigramFrequencySocketRepository;
import com.blindscenes.bigrammer.infrastructure.sockets.SocketServer;
import com.blindscenes.bigrammer.infrastructure.sockets.SocketClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import com.blindscenes.bigrammer.UIConsole;
import com.blindscenes.bigrammer.domain.*;
import com.blindscenes.bigrammer.Infrastructure.*;
import com.blindscenes.bigrammer.Infrastructure.rmi.RMIClient;
import com.blindscenes.bigrammer.Infrastructure.rmi.BigramFrequencyRMIRepository;
import com.blindscenes.bigrammer.domain.scanner.Scanner;
import com.blindscenes.bigrammer.domain.parser.Parser;
import com.blindscenes.bigrammer.crosscuttingconcerns.Log;
import com.blindscenes.bigrammer.crosscuttingconcerns.FileLog;
import com.blindscenes.bigrammer.infrastructure.rmi.BigramFrequencyRMIFacade;
import com.blindscenes.bigrammer.infrastructure.rmi.BigramFrequencyRMIService;
import com.blindscenes.bigrammer.infrastructure.rmi.RMIServer;


/**
 *
 * @author cristian
 * Take notes from the next post: http://www.yegor256.com/2014/10/03/di-containers-are-evil.html
 * Providers implemented to avoid dirty code with annotations.
 */
public class RegisterServices extends AbstractModule {

    @Override
    protected void configure() {
               
        super.bind(BigramFrequencyRepository.class)
                .annotatedWith(Names.named("rmiRepository"))
                .to(BigramFrequencyRMIRepository.class);
        
        super.bind(ClientService.class)
                .annotatedWith(Names.named("rmiClient"))
                .to(RMIClient.class);
                //.in(Singleton.class);
        
        super.bind(ServerService.class)
                .annotatedWith(Names.named("rmiServer"))
                .to(RMIServer.class);
        
        super.bind(BigramFrequencyRMIFacade.class)
                .annotatedWith(Names.named("rmiService"))
                .to(BigramFrequencyRMIService.class);        
        
        super.bind(ClientService.class)
                .annotatedWith(Names.named("socketClient"))
                .to(SocketClient.class);

        super.bind(BigramFrequencyRepository.class)
                .annotatedWith(Names.named("mapRepository"))
                .to(BigramFrequencyMapRepository.class);
        
        super.bind(ServerService.class)
                .annotatedWith(Names.named("socketServer"))
                .to(SocketServer.class);
                
        super.bind(BigramFrequencyRepository.class)
                .annotatedWith(Names.named("socketRepository"))
                .to(BigramFrequencySocketRepository.class);
                       
        super.bind(UIConsole.class)
                .in(Singleton.class);
        
        super.bind(Parser.class)
                .toProvider(BigramsParserProvider.class);
                
        super.bind(Scanner.class)
                .annotatedWith(Names.named("cleanScanner"))
                .toProvider(CleanScannerProvider.class);

        super.bind(Scanner.class)
                .annotatedWith(Names.named("byMatchingScanner"))
                .toProvider(ByMatchingScannerProvider.class);

        super.bind(CharSequence.class)
                .annotatedWith(Names.named("texto"))
                .toInstance(readFile());

        super.bind(Gson.class)
                .toInstance(new GsonBuilder().setPrettyPrinting().create());
        
        super.bind(Log.class)
                .to(FileLog.class)
                .in(Singleton.class);
    }

    private CharSequence readFile() {
        try {
            return new String(Files.readAllBytes(Paths.get("Sample.txt")));//corpus35MillonesDeTweets.csv Sample.txt
        } catch (IOException ex) {
            return ""; //"No Data/Error"
        }
    }

}
