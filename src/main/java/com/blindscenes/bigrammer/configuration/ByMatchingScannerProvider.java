/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.configuration;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.blindscenes.bigrammer.domain.scanner.ByMatchingScanner;

/**
 *
 * @author cristian
 */
public class ByMatchingScannerProvider implements Provider<ByMatchingScanner> {
    
    private final CharSequence charSequence;
    @Inject
    public ByMatchingScannerProvider(@Named("texto") CharSequence charSequence) {
        assert charSequence != null;
        this.charSequence = charSequence;
    }
    
    @Override
    public ByMatchingScanner get() {
        return new ByMatchingScanner(this.charSequence); 
    }
    
}
