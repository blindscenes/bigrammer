/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.configuration;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.name.Named;
import com.blindscenes.bigrammer.domain.scanner.CleanScanner;
import com.blindscenes.bigrammer.domain.scanner.Scanner;

/**
 *
 * @author cristian
 */
public class CleanScannerProvider implements Provider<CleanScanner>{
    
    private final Scanner scanner;
    
    @Inject
    public CleanScannerProvider(@Named("byMatchingScanner") Scanner scanner){
        assert scanner != null;
        this.scanner = scanner;
    }
    
    @Override
    public CleanScanner get() {
        return new CleanScanner(scanner);
    }
    
}
