/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.infrastructure.sockets;

import com.blindscenes.bigrammer.domain.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.io.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


/**
 *
 * @author cristian
 */
public class SocketServer implements  ServerService {

    private final short PORT = 8095;
    private final BigramFrequencyRepository repository;
    private ServerSocket serverSocket;
    
    @Inject
    public SocketServer(@Named("mapRepository") BigramFrequencyRepository repository){
        assert repository !=  null;
        this.repository = repository;
    }
    
    @Override
    public void start() {
         Thread thread = new Thread(() -> {
            try(ServerSocket serverSocket = new ServerSocket(PORT, 5)) {
                this.serverSocket = serverSocket;
                listening(serverSocket);

            } catch (IOException ex) {
                System.err.print(ex);
            }
         });
         thread.start();
    }

    @Override
    public void stop() {
        try {
            serverSocket.close();
        } catch (IOException ex) {
            System.err.print(ex);
        }
    }
    
    
    private void listening(ServerSocket serverSocket) throws IOException{
       
        while(!serverSocket.isClosed()){
            Socket socket = serverSocket.accept();
            
            Thread thread = new Thread(() -> {
                talking(socket);
            });
            thread.start();
        }
    }
    
    private void talking(Socket socket){
        try(Socket socketd = socket;
            BufferedReader in = new BufferedReader(new InputStreamReader(socketd.getInputStream()));
            ObjectOutputStream out = new ObjectOutputStream(socketd.getOutputStream())){
            
            String inputLine;
            BigramFrequency bigramFrequency;
            while ((inputLine = in.readLine()) != null) {
                bigramFrequency = this.repository.getBigramFrequency(inputLine);
                out.writeObject(bigramFrequency);
                out.flush();
            }
        } catch (IOException ex) {
            System.err.print(ex);
        }
    }
}
