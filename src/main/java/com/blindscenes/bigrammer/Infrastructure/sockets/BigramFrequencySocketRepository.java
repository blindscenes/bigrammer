/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.Infrastructure.sockets;

import com.blindscenes.bigrammer.domain.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 *
 * @author cristian
 */
public class BigramFrequencySocketRepository implements BigramFrequencyRepository {
    private final ClientService client;
    
    @Inject
    public BigramFrequencySocketRepository(@Named("socketClient")ClientService client){
        assert client != null;
        this.client = client;
    }
    
    
    @Override
    public BigramFrequency getBigramFrequency(String bigram) {
        return client.getRemoteBigramFrequency(bigram);
    }
    
}
