/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.infrastructure.sockets;

import com.blindscenes.bigrammer.domain.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author cristian
 */
public class SocketClient  implements ClientService {

    private final short PORT = 8095;
    private final String HOST = "localhost";

    @Override
    public BigramFrequency getRemoteBigramFrequency(String bigram) {
        try(Socket socket = new Socket(HOST, PORT);) {

            return talking(socket, bigram);
            
        } catch (IOException ex) {
            System.err.print(ex);
            return null;
        }
    }
    
    public BigramFrequency talking(Socket socket, String bigram){
        try(ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true)){
            
            out.println(bigram);
            BigramFrequency bigramFrequency = (BigramFrequency)in.readObject();

            return bigramFrequency;
            
        } catch (IOException | ClassNotFoundException ex) {
            System.err.print(ex);
            return null;
        }
    }
}
