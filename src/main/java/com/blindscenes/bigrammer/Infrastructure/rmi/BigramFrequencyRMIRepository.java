/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.Infrastructure.rmi;

import com.blindscenes.bigrammer.domain.BigramFrequency;
import com.blindscenes.bigrammer.domain.BigramFrequencyRepository;
import com.blindscenes.bigrammer.domain.ClientService;
import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 *
 * @author cristian
 */
public class BigramFrequencyRMIRepository implements BigramFrequencyRepository {
    
    private final ClientService client;
    
    @Inject
    public BigramFrequencyRMIRepository(@Named("rmiClient") ClientService client){
        assert client != null;
        this.client = client;
    }
            
    @Override
    public BigramFrequency getBigramFrequency(String bigram) {
        assert bigram != null;
        return client.getRemoteBigramFrequency(bigram);
    }
    
}
