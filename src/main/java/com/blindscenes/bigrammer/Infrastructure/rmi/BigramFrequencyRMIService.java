/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.infrastructure.rmi;

import com.blindscenes.bigrammer.domain.BigramFrequency;
import com.blindscenes.bigrammer.domain.BigramFrequencyRepository;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author cristian
 */
public class BigramFrequencyRMIService extends UnicastRemoteObject  implements BigramFrequencyRMIFacade {

    private final BigramFrequencyRepository repository;

    
    @Inject
    public BigramFrequencyRMIService(@Named("mapRepository") BigramFrequencyRepository repository) throws RemoteException{
        assert repository !=  null;
        this.repository = repository;
    }
    
    @Override
    public BigramFrequency getBigramFrequency(String bigram) throws RemoteException {
        return repository.getBigramFrequency(bigram);
    }
    
}
