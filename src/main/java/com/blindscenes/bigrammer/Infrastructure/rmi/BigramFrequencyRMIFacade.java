/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.infrastructure.rmi;

import java.rmi.Remote;
import com.blindscenes.bigrammer.domain.*;
import java.rmi.RemoteException;

/**
 *
 * @author cristian
 */
public interface BigramFrequencyRMIFacade extends Remote {

    BigramFrequency getBigramFrequency(String bigram) throws RemoteException;

}
