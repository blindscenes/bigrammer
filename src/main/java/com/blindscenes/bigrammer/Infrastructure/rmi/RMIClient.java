/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.Infrastructure.rmi;

import com.blindscenes.bigrammer.domain.BigramFrequency;
import com.blindscenes.bigrammer.domain.ClientService;
import com.blindscenes.bigrammer.infrastructure.rmi.BigramFrequencyRMIFacade;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cristian
 */
public class RMIClient implements ClientService {

    BigramFrequencyRMIFacade rmiClient;
    
    public RMIClient() {
        try {
            rmiClient = (BigramFrequencyRMIFacade) Naming.lookup("//localhost/RMIServer");
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            System.err.println(ex);
        }
    }
    
    @Override
    public BigramFrequency getRemoteBigramFrequency(String bigram) {
        try {
            return rmiClient.getBigramFrequency(bigram);
        } catch (RemoteException ex) {
            System.err.println(ex);
        }
        return null;
    }
    
}
