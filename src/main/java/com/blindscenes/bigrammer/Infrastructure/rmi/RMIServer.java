/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.infrastructure.rmi;

import com.blindscenes.bigrammer.domain.*;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cristian
 */
public class RMIServer implements ServerService{
    
    BigramFrequencyRMIFacade rmiService;
    
    @Inject 
    public RMIServer(@Named("rmiService") BigramFrequencyRMIFacade rmiService){
        assert rmiService != null;
        this.rmiService = rmiService;
    }
    
    @Override
    public void start() {
        try {
            Naming.rebind("//localhost/RMIServer", rmiService);
        } catch (RemoteException | MalformedURLException ex) {
            Logger.getLogger(RMIServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void stop() {
        try {
            Naming.unbind("//localhost/RMIServer");
        } catch (RemoteException | NotBoundException | MalformedURLException ex) {
            Logger.getLogger(RMIServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
