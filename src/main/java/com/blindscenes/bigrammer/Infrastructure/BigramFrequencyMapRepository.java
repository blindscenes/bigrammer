/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.Infrastructure;

import com.blindscenes.bigrammer.domain.BigramFrequency;
import com.blindscenes.bigrammer.domain.BigramFrequencyRepository;
import com.blindscenes.bigrammer.domain.parser.Parser;
import com.google.inject.Inject;
import java.util.Map;

/**
 *
 * @author cristian
 */
public class BigramFrequencyMapRepository implements  BigramFrequencyRepository{

    private Map map;
    
    @Inject
    public BigramFrequencyMapRepository(Parser parser){
        assert parser != null;       
        this.map = parser.getBigrams();
    }
    
    @Override
    public BigramFrequency getBigramFrequency(String bigram) {
        int freq = map.containsKey(bigram) ? (int)map.get(bigram) : 0;
        return new BigramFrequency(bigram, freq);
    }
    
}
