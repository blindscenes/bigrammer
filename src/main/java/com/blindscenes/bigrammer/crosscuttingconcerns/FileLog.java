/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blindscenes.bigrammer.crosscuttingconcerns;

import com.google.gson.Gson;
import com.google.inject.Inject;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.*;


/**
 *
 * @author cristian
 */
public class FileLog implements Log {

    private final Gson gson;

    /**
     * Insert a serialize factory
     *
     * @param gson
     */
    @Inject
    public FileLog(Gson gson) {
        assert gson != null;
        this.gson = gson;
    }

    @Override
    public void Save(Object pojo) {
        assert pojo instanceof Map;
        pojo = getOrdered((Map<String, Integer>)pojo);
        String text = gson.toJson(pojo);//, fooType);
        Save(text);
    }

    //refactor this
    private void Save(String text) {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("bigrams.json"), "utf-8"))) {
            writer.write(text);
        } catch (Exception e) {

        }
    }

    private TreeMap<String, Integer> getOrdered(Map<String, Integer> map) {
        TreeMap<String, Integer> bigrams = new TreeMap((bigram1, bigram2) -> ((String) bigram1).compareTo((String) bigram2));
        bigrams.putAll(map);
        return bigrams;
    }
}
